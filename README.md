# Node.js OpenAPI implemented by TSOA

TSOA documentation <https://tsoa-community.github.io/docs/>

## Goals

- TSOA template with Express
- Swagger UI
- Supported authentication
    - ApiKey
    - BasicAuth
    - JWT
- Role-based access control
- Loading auth from static conf or from DB
- Automatically rebuild on code change

## Development

```sh
npm install
npm run dev
open http://localhost:3000
```
