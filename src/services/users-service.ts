import { User } from "../models/user";

// A post request should not contain an id.
export type UserCreationParams = Pick<User, "email" | "name" | "phoneNumbers" | "scopes">;

const users: User[] = [];

export class UsersService {

    public selectAll(): User[] {
        return users;
    }

    public selectByIdOrName(id: number, name?: string): User | null {
        return users.find(u => u.id === id) || null;
    }

    public create(userCreationParams: UserCreationParams): User {
        const user: User = {
            id: Math.floor(Math.random() * 10000),
            status: "Happy",
            ...userCreationParams,
        };
        users.push(user);
        return user;
    }
}
