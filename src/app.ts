import express from "express";
import nconf from "nconf";
import path from "path";
import { objPaths } from "peryl/dist/objpaths";
import { api } from "./api";
import { ResolveApiKey, ResolveUser } from "./api/authentication";
import { Conf, confDefault } from "./conf";
const pkg = require(path.resolve(__dirname, "../package.json"));

nconf
    .argv()
    .env({
        separator: "_",
        lowerCase: false,
        whitelist: objPaths(confDefault).map(c => c.join("__")),
        parseValues: true,
    })
    .file(path.join(__dirname, "..", "conf.json"))
    .defaults(confDefault);

export const conf = nconf.get() as Conf;

console.log("NODE_ENV", process.env.NODE_ENV);
console.log("version", pkg.version);
console.log("conf", JSON.stringify(conf));

export const app = express();

const resolveUser: ResolveUser = conf.auth.basic.subs
        ? async (username: string, password: string) =>
            conf.auth.basic.subs![username].password === password
                ? { sub: username, scopes: conf.auth.basic.subs![username].scopes }
                : undefined
        : async (_: string) => undefined; // NOTE: Custom method. e.g. loading auth from db
const resolveApiKey: ResolveApiKey = conf.auth.apiKey
    ? async (apiKey: string) => conf.auth.apiKey![apiKey]
    : async (_: string) => undefined; // NOTE: Custom method. e.g. loading auth from db

const apiRouter = api(resolveUser, resolveApiKey, conf);

app.use("/api", apiRouter);

app.get("/", (req, res) => res.redirect("/api"));
