export const confDefault = {
    auth: {
        apiKey: {
            admin_key: { sub: "admin" },
            user_key: { sub: "user", scopes: ["user:view", "user:create"] },
            test_key: { sub: "test", scopes: ["user:view"] }
        } as { [api_key: string]: { sub: string; scopes?: string[]; } } | undefined,
        basic: {
            subs: {
                admin: { password: "adminpwd" },
                user: { password: "userpwd", scopes: ["user:view", "user:create"] },
                test: { password: "testpwd", scopes: ["user:view"] }
            },
            challenge: true,
            realm: "api",
        } as {
            subs?: {
                [subject: string]: { password: string; scopes?: string[] };
            };
            challenge: boolean;
            realm: string;
        },
        jwt: {
            secretOrPrivateKey: "TEST",
            secretOrPublicKey: "TEST",
            expiretion: 100,
            algorithms: ["HS256"],
            tokenClaimSubject: "sub",
            tokenClaimScope: "scope"
        }
    }
};

export type Conf = typeof confDefault;
