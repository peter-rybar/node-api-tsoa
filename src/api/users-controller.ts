import {
    Body,
    Controller,
    Get,
    Path,
    Post,
    Query,
    Request,
    Route,
    Security,
    SuccessResponse,
    Tags
} from "tsoa";
import { User } from "../models/user";
import { UserCreationParams, UsersService } from "../services/users-service";

/**
 * Operations about users.
 *
 * Find out more about users
 */
@Route("/users")
@Tags("Users")
export class UsersController extends Controller {

    @Get("/logged")
    @Security("api_key", ["user:create"])
    @Security("basic_auth", ["user:create"])
    @Security("jwt", ["user:create"])
    public async getLogged(@Request() req: Request): Promise<any> {
        console.log("Users getLogged:", (req as any).user);
        return (req as any).user;
    }

    /**
     * Retrieves the details of an existing user.
     * Supply the unique user ID from either and receive corresponding user details.
     *
     * @param userId The user's identifier
     * @param name Provide a username to display
     */
    @Get("/")
    public async getUsers(): Promise<User[]> {
        console.log("Users getUsers");
        return new UsersService().selectAll();
    }

    @Get("/{userId}")
    @Security("api_key", ["user:view"])
    @Security("basic_auth", ["user:view"])
    @Security("jwt", ["user:view"])
    public async getUser(
        @Path() userId: number,
        @Query() name?: string
    ): Promise<User | null> {
        console.log("Users getUser:", userId, name);
        return new UsersService().selectByIdOrName(userId, name);
    }

    @Post("/")
    @Security("api_key", ["user:create"])
    @Security("basic_auth", ["user:create"])
    @Security("jwt", ["user:create"])
    @SuccessResponse("201", "Created") // Custom success response
    public async postUser(
        @Body() requestBody: UserCreationParams
    ): Promise<User> {
        console.log("Users postUser:", requestBody);
        this.setStatus(201); // set return status 201
        return new UsersService().create(requestBody);
    }
}
