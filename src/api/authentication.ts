import basicAuth from "basic-auth";
import { Request as Req } from "express";
import jwt from "jsonwebtoken";
import { Conf } from "../conf";

export interface UserAuth {
    sub: string;
    scopes?: string[];
}

export type ResolveUser = (username: string, password: string) => Promise<{ sub: string; scopes?: string[]; } | undefined>;
export type ResolveApiKey = (apiKey: string) => Promise<{ sub: string; scopes?: string[]; } | undefined>;

let _confAuth: Conf["auth"];
let _resolveUser: ResolveUser;
let _resolveApiKey: ResolveApiKey;

export function initAuthentication(conf: Conf["auth"],
                                   resolveUser: ResolveUser,
                                   resolveApiKey: ResolveApiKey): void {
    _confAuth = conf;
    _resolveUser = resolveUser;
    _resolveApiKey = resolveApiKey;
}

export async function expressAuthentication(req: Req, securityName: string, scopes?: string[]): Promise<any> {
    console.debug("TSOA expressAuthentication:", securityName, scopes);
    switch (securityName) {

        case "api_key": {
            const apiKey = (req.query && req.query.api_key) || req.headers["x-api-key"];
            if (typeof apiKey === "string") {
                const user = await _resolveApiKey(apiKey);
                if (user) {
                    // NOTE: Admin subjects have no scopes = []
                    const subScopes: string[] = user.scopes ?? [];
                    if (scopes && subScopes.length) {
                        for (const scope of scopes) {
                            if (!subScopes.includes(scope)) {
                                const msg = "API Key does not have required scope";
                                console.warn("Authentication:", securityName, user, msg, scope);
                                return Promise.reject(new Error(msg));
                            }
                        }
                    }
                    const userAuth: UserAuth = {
                        sub: user.sub,
                        scopes: user.scopes
                    };
                    console.log("Authentication:", securityName, userAuth, user);
                    return Promise.resolve(userAuth);
                } else {
                    const msg = "Invalid API Key";
                    console.warn("Authentication:", securityName, msg, user);
                    return Promise.reject(new Error(msg));
                }
            } else {
                const msg = "Malformed API Key";
                console.debug("Authentication:", securityName, msg);
                return Promise.reject(new Error(msg));
            }
        }

        case "basic_auth": {
            const auth = basicAuth(req);
            if (auth) {
                const user = await _resolveUser(auth.name, auth.pass);
                if (user) {
                    // NOTE: Admin subjects have no scopes = []
                    const subScopes: string[] = user.scopes ?? [];
                    if (subScopes && subScopes.length) {
                        for (const scope of subScopes) {
                            if (!subScopes.includes(scope)) {
                                const msg = "User does not have required scope";
                                console.warn("Authentication:", securityName, auth, msg, scope, subScopes);
                                return Promise.reject(new Error(msg));
                            }
                        }
                    }
                    const userAuth: UserAuth = {
                        sub: user.sub,
                        scopes: subScopes
                    };
                    console.log("Authentication:", securityName, userAuth, auth);
                    return Promise.resolve(userAuth);
                } else {
                    const msg = "Invalid basic auth credentials";
                    console.warn("Authentication:", securityName);
                    return Promise.reject(new Error(msg));
                }
            } else {
                const msg = "No basic auth credentials";
                console.debug("Authentication:", securityName, msg, auth);
                return Promise.reject(new Error(msg));
            }
        }

        case "jwt": {
            const token = parseBearerToken(req) || req.headers["x-access-token"] || req.query.token || req.body.token;
            if (token) {
                return new Promise<any>((resolve, reject) => {
                    jwt.verify(token, _confAuth.jwt.secretOrPublicKey, (err: any, decoded: any) => {
                        if (err) {
                            console.warn("Authentication:", securityName, err.message);
                            return reject(err);
                        } else {
                            const subScopes = decoded[_confAuth.jwt.tokenClaimScope];
                            if (scopes) {
                                if (subScopes) {
                                    const scopeIntersection = intersection(scopes, subScopes);
                                    console.debug("Authentication:", securityName, scopes, subScopes, scopeIntersection);
                                    if (!scopeIntersection.length) {
                                        const msg = "JWT does not contain required scope";
                                        console.warn("Authentication:", securityName, ":", msg);
                                        return reject(new Error(msg));
                                    }
                                    // for (const scope of scopes) {
                                    //     if (!subScopes.includes(scope)) {
                                    //         const msg = "JWT does not contain required scope";
                                    //         console.warn("Authentication:", securityName, msg, subScopes, scope);
                                    //         return reject(new Error(msg));
                                    //     }
                                    // }
                                }
                                // ELSE: Admin subjects have no scopes = ""
                            }
                            const userAuth: UserAuth = {
                                sub: decoded[_confAuth.jwt.tokenClaimSubject],
                                scopes: subScopes
                            };
                            console.log("Authentication:", securityName, userAuth, decoded);
                            return resolve(userAuth);
                        }
                    });
                });
            } else {
                const msg = "No JWT token";
                console.debug("Authentication:", securityName, msg);
                return Promise.reject(new Error(msg));
            }
        }

        default: {
            const msg = `Unsupported authentication: ${securityName}`;
            console.debug("Authentication:", securityName, msg);
            return Promise.reject(new Error(msg));
        }
    }
}

function parseBearerToken(req: Req): string | undefined {
    if (req.headers.authorization && req.headers.authorization.split(" ")[0] === "Bearer") {
        return req.headers.authorization.split(" ")[1];
    }
    return undefined;
}

function intersection(arr1: string[], arr2: string[]) {
    return arr1.filter(elem => arr2.includes(elem));
}
