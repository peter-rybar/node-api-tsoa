import { NextFunction, Request, Response } from "express";
import { ValidateError } from "tsoa";

export function errorHandler(err: unknown, req: Request, res: Response, next: NextFunction): Response | void {
    if (err instanceof ValidateError) {
        console.warn(`Caught Validation Error for ${req.path}:`, err.fields);
        return res.status(422).json({
            message: "Validation Failed",
            details: err?.fields,
        });
    }
    if (err instanceof Error) {
        const status = (err as any).status || (err as any).statusCode || 500;
        const message = err.message;
        if (err.stack) {
            console.error({
                status,
                message,
                stack: err.stack
            });
        } else {
            console.error({
                status,
                message,
                error: err
            })
        }
        return res
            .status(status)
            .json({
                error: err.message || "Internal Server Error",
            });
    }
    next();
}